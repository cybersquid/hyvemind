﻿using UnityEngine;

namespace Net.Cybersquid.Hyvemind
{
    public class AutonomousAgent : MonoBehaviour
    {
        public string type = "Agent";

        // Movement
        [HideInInspector]
        public Vector2 velocity = Vector2.zero;
        [HideInInspector]
        public Vector2 acceleration = Vector2.zero;

        protected float maxSpeed = 4.0f;
        protected float maxForce = .1f;

        // Seek
        protected GameObject target;
        protected float seekWeight = 1.0f;
        protected float arrivalRadius = 0.5f;


        public void SetMovement(float maxSpeed, float maxForce)
        {
            this.maxSpeed = maxSpeed;
            this.maxForce = maxForce;
        }

        public void SetTarget(GameObject target, float seekWeight, float arrivalRadius)
        {
            this.target = target;
            this.seekWeight = seekWeight;
            this.arrivalRadius = arrivalRadius;
        }

        public virtual void ApplyBehaviours()
        {
            if (target)
                Seek();

            Move();
        }

        public void Move()
        {
            // add the sum of all steering forces to the current velocity
            velocity += acceleration;
            // clamp the magnitude of the current velocity to not exceed the max speed
            velocity = Vector2.ClampMagnitude(velocity, maxSpeed);
            // apply the new velocity vector to move the vehicle
            transform.Translate(velocity * Time.deltaTime);
            // reset all steering forces (that are combined in the acceleration)
            acceleration *= 0;
        }

        /// <summary>
        /// Adds a (steering) force vector to the current acceleration.
        /// The acceleration is the entirety of all steering forced applied to the vehicle in one update cicle
        /// </summary>
        /// <param name="force">steering vector added to the current acceleration of the agent</param>
        public void ApplyForce(Vector2 force)
        {
            acceleration += force;
        }

        public void Seek()
        {
            Seek(target, arrivalRadius, seekWeight);
        }

        /// <summary>
        /// Seek is the basic function of every steering behaviour. It gives an AutonomousAgent a target to steer towards to using the steering formular of Craig Reynolds to make the movement appear more natural.
        /// </summary>
        /// <param name="target">Target GameObject the agent is steering towards</param>
        /// <param name="arrivalRadius">Radius of the circular area around the target in which the agent is slowing down its movement until it finally reaches the position of the target.</param>
        /// <param name="weight">Defines how "important" this behaviour is compared to the other behaviours. Affects (scales) the magnitude of the steering vector as a multiplier. 0 means the behaviour is not applied.</param>
        public void Seek(GameObject target, float arrivalRadius, float weight = 1.0f)
        {
            if (!target)
                return;

            Seek(target.transform.position, arrivalRadius, weight);
        }

        /// <summary>
        /// Seek is the basic function of every steering behaviour. It gives an AutonomousAgent a target to steer towards to using the steering formular of Craig Reynolds to make the movement appear more natural.
        /// </summary>
        /// <param name="target">Target position the agent is steering towards</param>
        /// <param name="arrivalRadius">Radius of the circular area around the target in which the agent is slowing down its movement until it finally reaches the position of the target.</param>
        /// <param name="weight">Defines how "important" this behaviour is compared to the other behaviours. Affects (scales) the magnitude of the steering vector as a multiplier. 0 means the behaviour is not applied.</param>
        public void Seek(Vector3 target, float arrivalRadius, float weight = 1.0f)
        {
            // desired velocity - vector from the current position of the vehicle to the target
            Vector2 desired = target - transform.position;

            // the magnitude of the desired velocity can be stored as the distance between the vehicle and the target
            float distance = desired.magnitude;

            if (distance < arrivalRadius)
            {
                // calculate the desired velocity to target in a mapped range from zero to max speed according to the vehicles distance to the target
                float magnitude = Utility.Map(distance, 0, arrivalRadius, 0, maxSpeed);
                desired = desired.normalized * magnitude;
            }
            else
            {
                // calculate the desired velocity to target at max speed
                desired = desired.normalized * maxSpeed;
            }

            // Craig Reynolds formular for steering force
            Vector2 steer = desired - velocity;

            // Clamp the steering to a max force to simulate a "steering ability" of the vehicle (is it a sluggish or an agile creature) 
            steer = Vector2.ClampMagnitude(steer, maxForce);

            // weight the behavior (how important is it compared to the other behaviors applied to the vehicle)
            steer *= weight;

            // add the steering to the current acceleration
            ApplyForce(steer);
        }
    }
}