﻿using UnityEngine;

namespace Net.Cybersquid.Hyvemind
{
    public class Wanderer : IndividualEntity
    {
        [Header("Wander")]
        public float wanderWeight = 1.0f;
        public float foresightDistance = 4.0f;
        public float foresightRadius = 2.0f;

        void Start()
        {
            SyncInternalWithExposedValues();
        }

        void Update()
        {

#if UNITY_EDITOR
            SyncInternalWithExposedValues();
#endif
            Wander();
            Move();
        }

        public void Wander()
        {
            Wander(wanderWeight);
        }

        public void Wander(float weight)
        {
            Vector2 foresight = (Vector2)transform.position + (velocity.normalized * foresightDistance);

            // caclulate random point on a unit circle circumference (the point will be located in the center of the world at x:0 y:0)
            float angle = Random.Range(0.0f, 1.0f) * Mathf.PI * 2;
            Vector2 randomVector = new Vector2(Mathf.Cos(angle) * foresightRadius, Mathf.Sin(angle) * foresightRadius);

            // translate the positon of the random vector to be located around the circumference of the circle around the foresight vector
            Vector2 target = randomVector + foresight;

            Debug.DrawLine(transform.position, foresight, Color.blue);
            Debug.DrawLine(foresight, target, Color.green);

            Seek(target, weight);
        }
    }
}
