﻿using UnityEngine;

namespace Net.Cybersquid.Hyvemind
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class Toroid2D : MonoBehaviour
    {
        private SpriteRenderer spriteRenderer;
        private bool isWrappingX;
        private bool isWrappingY;

        void Start()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();

            if (!Camera.main)
                Debug.LogWarning("Toroid2D: No Camera with tag \"MainCamera\" was found in the scene. Toroidal Screen Wrap is not active.");
        }

        void Update()
        {
            ScreenWrap();
        }

        /// <summary>
        /// Simulates a toroidal screen by switching the position of entities that are no longer visible on the main camera (by checking if their spriterenderer is still visible)
        /// build using an online article under: http://gamedevelopment.tutsplus.com/articles/create-an-asteroids-like-screen-wrapping-effect-with-unity--gamedev-15055
        /// </summary>
        public void ScreenWrap()
        {
            if (!Camera.main)
                return;

            if (spriteRenderer.isVisible)
            {
                isWrappingX = false;
                isWrappingY = false;
                return;
            }

            if (isWrappingX && isWrappingY)
            {
                return;
            }

            // if the program is launched in the editor Unity will use the "Sceen" camera - not that cool but the wrapping is only for testing purpose
            var viewportPosition = Camera.main.WorldToViewportPoint(transform.position);
            var newPosition = transform.position;

            if (!isWrappingX && (viewportPosition.x > 1 || viewportPosition.x < 0))
            {
                newPosition.x = -newPosition.x;
                isWrappingX = true;
            }

            if (!isWrappingY && (viewportPosition.y > 1 || viewportPosition.y < 0))
            {
                newPosition.y = -newPosition.y;
                isWrappingY = true;
            }

            transform.position = newPosition;
        }
    }
}