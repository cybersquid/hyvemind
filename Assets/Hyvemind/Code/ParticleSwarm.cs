﻿using UnityEngine;

namespace Net.Cybersquid.Hyvemind
{
    [RequireComponent(typeof(ParticleSystem))]
    public class ParticleSwarm : MonoBehaviour
    {
        [HideInInspector]
        public Vector2 velocity;
        [HideInInspector]
        public Vector2 acceleration;

        [Header("Movement")]
        public float maxSpeed = 4.0f;
        public float maxForce = .1f;

        [Header("Seeking Behaviour")]
        public GameObject target;
        public float seekWeight = 1.0f;
        public float arrivalRadius = 0.5f;

        [Header("Flocking Behaviour")]
        public WeightedBehaviour seperation;
        public WeightedBehaviour alignment;
        public WeightedBehaviour cohesion;

        [Header("Avoidance")]
        public WeightedBehaviour avoidance;
        public GameObject[] obstacles;

        [HideInInspector]
        public ParticleSystem System;
        [HideInInspector]
        public ParticleSystem.Particle[] Particles;

        void Awake()
        {
            Initialize();
        }

        void Update()
        {
            // GetParticles is allocation free because we reuse the Particles buffer between updates
            int numParticlesAlive = System.GetParticles(Particles);

            // Change only the particles that are alive
            for (int i = 0; i < numParticlesAlive; i++)
            {
                ApplyBehaviours(ref Particles[i]);
            }

            // Apply the particle changes to the particle system
            System.SetParticles(Particles, numParticlesAlive);
        }

        /// <summary>
        /// Initialize the ParticleSystem and the Particle Buffer 
        /// Build on the Unity 3D Tutorial: https://docs.unity3d.com/ScriptReference/ParticleSystem.GetParticles.html
        /// </summary>
        private void Initialize()
        {
            if (System == null)
                System = GetComponent<ParticleSystem>();

            if (Particles == null || Particles.Length < System.main.maxParticles)
            {
                Particles = new ParticleSystem.Particle[System.main.maxParticles];
            }
        }

        /// <summary>
        /// Apply Steering behaviour to calculate a new acceleration for the current particle.
        /// </summary>
        /// <param name="particle">Currently processed particle</param>
        protected void ApplyBehaviours(ref ParticleSystem.Particle particle)
        {
            // chase target
            Seek(target, particle.position, seekWeight);

            // apply swarm behaviour (Seperation, Alignment, Cohesion)
            Flock(particle);

            // obstacle avoidance
            if (obstacles.Length > 0)
            {
                Seperate(particle, obstacles, avoidance.radius, avoidance.weight);
            }

            // Apply the final acceleration to the particle velocity
            particle.velocity += (Vector3)acceleration;
            particle.velocity = Vector3.ClampMagnitude(particle.velocity, maxSpeed);

            // reset velocity
            acceleration *= 0;
        }

        protected void ApplyForce(Vector2 force)
        {
            acceleration += force;
        }

        protected void Seek(GameObject target, Vector3 currentPos, float weight = 1.0f)
        {
            if (target == null)
                return;

            Seek(target.transform.position, currentPos, weight);
        }

        protected void Seek(Vector3 target, Vector3 currentPos, float weight = 1.0f)
        {
            target -= System.transform.position;
            // desired velocity - vector from the current position of the particle to the target
            Vector2 desired = target - currentPos;

            // the magnitude of the desired velocity can be stored as the distance between the particle and the target
            float distance = desired.magnitude;

            if (distance < arrivalRadius)
            {
                // calculate the desired velocity to target in a mapped range from zero to max speed according to the boids distance to the target
                float magnitude = Utility.Map(distance, 0, arrivalRadius, 0, maxSpeed);
                desired = desired.normalized * magnitude;
            }
            else
            {
                // calculate the desired velocity to target at max speed
                desired = desired.normalized * maxSpeed;
            }

            // Craig Reynold's formular for steering force
            Vector2 steer = desired - velocity;

            // Clamp the steering to a max force to simulate a "steering ability" of the particle (is it a sluggish or an agile creature) 
            steer = Vector2.ClampMagnitude(steer, maxForce);

            // weight the behavior (how important is it compared to the other behaviors applied to the particle)
            steer *= weight;

            // add the steering to the current acceleration
            ApplyForce(steer);
        }

        public void Flock(ParticleSystem.Particle currentParticle)
        {
            Vector3 sumSeperation = new Vector3();
            int countSeperation = 0;

            Vector3 sumAlignment = new Vector3();
            int countAlignment = 0;

            Vector3 sumCohesion = new Vector3();
            int countCohesion = 0;

            foreach (ParticleSystem.Particle otherParticle in Particles)
            {
                float distance = (otherParticle.position - currentParticle.position).magnitude;

                // Seperation
                if ((distance > 0) && (distance < seperation.radius))
                {
                    Vector3 diff = currentParticle.position - otherParticle.position;
                    diff.Normalize();
                    diff /= distance;

                    sumSeperation += diff;
                    countSeperation++;
                }

                // Alignment
                if ((distance > 0) && (distance < alignment.radius))
                {
                    sumAlignment += otherParticle.velocity;
                    countAlignment++;
                }

                // Cohesion
                if ((distance > 0) && (distance < cohesion.radius))
                {
                    sumCohesion += otherParticle.position;
                    countCohesion++;
                }
            }

            // Seperation
            if (countSeperation > 0 && seperation.weight > 0)
            {
                sumSeperation /= countSeperation;
                sumSeperation *= maxSpeed;

                Vector3 steer = sumSeperation - currentParticle.velocity;
                steer = Vector3.ClampMagnitude(steer, maxForce);
                steer *= seperation.weight;

                ApplyForce(steer);
            }

            // Alignment
            if (countAlignment > 0 && alignment.weight > 0)
            {
                sumAlignment /= countAlignment;
                sumAlignment = sumAlignment.normalized * maxSpeed;

                Vector3 steer = sumAlignment - currentParticle.velocity;
                steer = Vector3.ClampMagnitude(steer, maxForce);
                steer *= alignment.weight;

                ApplyForce(steer);
            }

            // Cohesion
            if (countCohesion > 0 && cohesion.weight > 0)
            {
                sumCohesion /= countCohesion;
                Seek(sumCohesion, currentParticle.position, cohesion.weight);
            }
        }

        protected void Seperate(ParticleSystem.Particle currentParticle, GameObject[] obstacles, float radius, float weight = 1.0f)
        {
            // We have to keep track how many boids (particles) are to close
            Vector2 sum = new Vector2();
            int count = 0;

            foreach (var obstacle in obstacles)
            {
                // distance between this particle and the currently processed particle in the list
                float distance = (obstacle.transform.position - currentParticle.position).magnitude;

                if ((distance > 0) && (distance < radius))
                {
                    // normalized vector that points away from the offending particle
                    Vector2 diff = currentParticle.position - obstacle.transform.position;
                    diff.Normalize();

                    // add a magnitude to the flee vector according to the distance of the offending particle
                    // the closer it is, the more we should flee
                    diff /= distance;

                    // add the current diff vector to the sum of all vectors pointing away from close particles
                    sum += diff;

                    count++;
                }
            }

            // if no other particle is to close nothing happens
            if (count > 0)
            {
                // if other particles are to close we use the sum and the count of all seperation vectors to calculate the average vector that points away from the offending particles 
                sum /= count;

                // scale the average vector to max speed -> this is our desired vector!
                sum *= maxSpeed;

                // Reynold's steering formular
                Vector2 steer = sum - velocity;

                // clamp the steering to a max force 
                steer = Vector2.ClampMagnitude(steer, maxForce);

                // weight the behavior (how important is it compared to the other behaviors applied to the particle)
                steer *= weight;

                ApplyForce(steer);
            }
        }
    }
}