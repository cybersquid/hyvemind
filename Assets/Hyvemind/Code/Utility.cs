﻿namespace Net.Cybersquid.Hyvemind
{
    public static class Utility
    {
        /// <summary>
        /// Re-maps a number from one range to another.
        /// </summary>
        /// <param name="value">the incoming value to be converted</param>
        /// <param name="oldMin">lower bound of the value's current range</param>
        /// <param name="oldMax">upper bound of the value's current range</param>
        /// <param name="newMin">lower bound of the value's target range</param>
        /// <param name="newMax">upper bound of the value's target range</param>
        /// <returns>value mapped to a new range</returns>
        public static float Map (float value, float oldMin, float oldMax, float newMin, float newMax)
        {
            float oldRange = (oldMax - oldMin);
            float newRange = (newMax - newMin);
            float newValue = (((value - oldMin) * newRange) / oldRange) + newMin;

            return newValue;
        }
    }
}