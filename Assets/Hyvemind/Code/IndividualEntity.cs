﻿using UnityEngine;

namespace Net.Cybersquid.Hyvemind
{
    public class IndividualEntity : AutonomousAgent
    {
        [Header("Movement")]
        public float MaxSpeed = 4.0f;
        public float MaxForce = .1f;

        [Header("Seek")]
        public GameObject Target;
        public float SeekWeight = 1.0f;
        public float ArrivalRadius = 0.5f;


        protected void SyncInternalWithExposedValues ()
        {
            maxSpeed = MaxSpeed;
            maxForce = MaxForce;

            target = Target;
            seekWeight = SeekWeight;
            arrivalRadius = ArrivalRadius;
        }
    }
}