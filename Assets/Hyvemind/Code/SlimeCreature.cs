﻿using UnityEngine;

namespace Net.Cybersquid.Hyvemind
{
    [RequireComponent(typeof(Animator))]
    public class SlimeCreature : MonoBehaviour
    {
        public bool randomAnimationStart;

        void Start()
        {
            if (randomAnimationStart)
                SetRandomAnimationStart();
        }

        private void SetRandomAnimationStart()
        {
            Animator anim = GetComponent<Animator>();
            if (!anim)
                return;

            AnimatorStateInfo state = anim.GetCurrentAnimatorStateInfo(0);
            anim.Play(state.fullPathHash, -1, Random.Range(0f, 1f));
        }
    }
}

