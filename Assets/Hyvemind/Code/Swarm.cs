﻿using System.Collections.Generic;
using UnityEngine;

namespace Net.Cybersquid.Hyvemind
{
    [System.Serializable]
    public struct WeightedBehaviour
    {
        public float weight;
        public float radius;
    }

    /// <summary>
    /// The core class of the Hyvemind package. Defines a collective of SwarmEntities which simulate the flocking bahaviour of a natural animal swarm (herd, flock). 
    /// Any swarm system describes an emergent phenomenon that appears to behave like one large living organism. Also called "Superorganism". 
    /// 
    /// Unity: "One by one, I will unify them, I will become the universe, and I will be what the single-minded once called a god."
    /// Rick: "I like that. Oh, that's pretty sexy. Hey, listen, where can we get a drink around here?"
    /// 
    /// - from "Rick and Morty" (TV Series 2015) - Season 2 Episode 3 - "Auto Erotic Assimilation"
    /// </summary>
    public class Swarm : MonoBehaviour
    {
        public static Dictionary<string, List<SwarmEntity>> entitiesByType;

        [HideInInspector]
        public List<SwarmEntity> entities = new List<SwarmEntity>();

        [Header("Entities")]
        public SwarmEntity entityPrefab;
        public int numberEntities;
        public float spawnRadius = 1.0f;
        public bool showPossibleSpawnPoints;

        [Header("Movement")]
        public float maxEntitySpeed = 4.0f;
        public float maxEntitySteeringForce = .1f;

        [Header("Seeking Behaviour")]
        public GameObject target;
        public float seekWeight = 1.0f;
        public float arrivalRadius = 0.5f;

        public WeightedBehaviour seperation;
        public WeightedBehaviour alignment;
        public WeightedBehaviour cohesion;

        [Header ("Predator Behaviour")]
        public WeightedBehaviour hunt;
        public string preyType;

        [Header("Prey Behaviour")]
        public WeightedBehaviour flee;
        public string[] fleeTypes;

        void Start()
        {
            Initialize();
        }

        void FixedUpdate()
        {
            foreach (SwarmEntity entity in entities)
            {
#if UNITY_EDITOR
                // needed to adjust values in the editor on play mode
                entity.SetMovement(maxEntitySpeed, maxEntitySteeringForce);
                entity.SetTarget(target, seekWeight, arrivalRadius);
#endif
                entity.ApplyBehaviours();
            }
        }

        /// <summary>
        /// Initial setup of the swarm. 
        /// Spawns the swarm entities on random start positions in the circular spawn area (depending on the entityPrefab and the numberof entities).
        /// Sets the movement and behaviour attributes for each spawend SwarmEntity.
        /// </summary>
        public virtual void Initialize()
        {
            for (int i = 0; i < numberEntities; i++)
            {
                SwarmEntity entity = Instantiate(entityPrefab, (Random.insideUnitCircle * spawnRadius) + (Vector2)transform.position, Quaternion.identity);
                entity.gameObject.transform.parent = transform;

                // set basic attributes for autonomous agents
                entity.SetMovement(maxEntitySpeed, maxEntitySteeringForce);
                entity.SetTarget(target, seekWeight, arrivalRadius);

                // set the swarm reference of the entity
                entity.Swarm = this;

                RegisterEntityByType(entity);
                entities.Add(entity);
            }
        }

        /// <summary>
        /// Adds a SwarmEntity to the "global" static list of entities assoziated by its specific type
        /// </summary>
        protected void RegisterEntityByType(SwarmEntity swarmEntity)
        {
            if (entitiesByType == null)
            {
                entitiesByType = new Dictionary<string, List<SwarmEntity>>();
            }
            if (entitiesByType.ContainsKey(swarmEntity.type) == false)
            {
                entitiesByType[swarmEntity.type] = new List<SwarmEntity>();
            }
            entitiesByType[swarmEntity.type].Add(swarmEntity);
        }

#if UNITY_EDITOR
        void OnDrawGizmos()
        {
            UnityEditor.Handles.color = Color.green;
            UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.forward, spawnRadius);

            if (!Application.isPlaying && showPossibleSpawnPoints)
            {
                for (int i = 0; i < numberEntities; i++)
                {
                    UnityEditor.Handles.DrawWireDisc((Random.insideUnitCircle * spawnRadius) + (Vector2)transform.position, Vector3.forward, 0.2f);
                }
            }
        }
#endif
    }
}