﻿using System.Collections.Generic;
using UnityEngine;

namespace Net.Cybersquid.Hyvemind
{
    /// <summary>
    /// Autonomouse agent that acts in a swarm collective of multiple SwarmEntities. 
    /// A SwarmEntity is able to make simple steering behaviour decissions depending on their own position and movement In relation to the position and movement of every other SwarmEntity of a swarm.
    /// The basic steering behaviours applied by any SwarmEntity are seperation, alignment and cohesion.
    /// </summary>
    public class SwarmEntity : AutonomousAgent
    {
        private Swarm _swarm;
        public Swarm Swarm
        {
            get { return _swarm; }
            set
            {
                if (_swarm == null)
                    _swarm = value;
            }
        }

        public override void ApplyBehaviours()
        {
            ApplyBehaviours(_swarm.entities);
        }

        /// <summary>
        /// Applies the movement to the swarm entities depending on the calculated steering behaviour for each individual entity.
        /// </summary>
        /// <param name="entities">the swarm entities that should be moved according to the calculated steering behaviour</param>
        public void ApplyBehaviours(List<SwarmEntity> entities)
        {
            // Flocking
            Seperate(entities);
            Align(entities);
            Cohesion(entities);

            // Predator Prey Behaviour
            Hunt();

            foreach (var fleeType in _swarm.fleeTypes)
            {
                Flee(fleeType);
            }

            // Searching
            if (target)
                Seek();

            Move();
        }


        public void Seperate()
        {
            Seperate(_swarm.entities);
        }

        public void Seperate(List<SwarmEntity> entities)
        {
            Seperate(entities, _swarm.seperation.radius, _swarm.seperation.weight);
        }

        /// <summary>
        /// Seperation simulates a natural flocking behaviour also known as "avoidance". Calculates a steering for every entity of the swarm to avoid a colliding with other nearby entities.
        /// </summary>
        /// <param name="entities">The entities of a swarm to which the behaviour should be applied.</param>
        /// <param name="radius">The radius of the circular area around each single entity in which the behaviour is effectiv</param>
        /// <param name="weight">Defines how "important" this behaviour is compared to the other behaviours. Affects (scales) the magnitude of the steering vector as a multiplier. 0 means the behaviour is not applied.</param>
        public void Seperate(List<SwarmEntity> entities, float radius, float weight = 1.0f)
        {
            if (entities == null)
                return;

            // We have to keep track how many entities are to close
            Vector2 sum = new Vector2();
            int count = 0;

            foreach (SwarmEntity entity in entities)
            {
                // distance between this entity and the currently processed entity in the list
                float distance = (entity.transform.position - transform.position).magnitude;

                if ((distance > 0) && (distance < radius))
                {
                    // normalized vector that points away from the offending entity
                    Vector2 diff = transform.position - entity.transform.position;
                    diff.Normalize();

                    // add a magnitude to the flee vector according to the distance of the offending entity
                    // the closer it is, the more we should flee
                    diff /= distance;

                    // add the current diff vector to the sum of all vectors pointing away from close entities
                    sum += diff;

                    count++;
                }
            }

            // if no other entity is to close nothing happens
            if (count > 0)
            {
                // if other entities are to close we use the sum and the count of all seperation vectors to calculate the average vector that points away from the offending entities 
                sum /= count;

                // scale the average vector to max speed -> this is our desired vector!
                sum *= maxSpeed;

                // Reynold's steering formular
                Vector2 steer = sum - velocity;

                // clamp the steering to a max force 
                steer = Vector2.ClampMagnitude(steer, maxForce);

                // weight the behavior (how important is it compared to the other behaviors applied to the entity)
                steer *= weight;

                ApplyForce(steer);
            }
        }

        public void Align()
        {
            Align(_swarm.entities);
        }

        public void Align(List<SwarmEntity> entities)
        {
            Align(entities, _swarm.alignment.radius, _swarm.alignment.weight);
        }

        /// <summary>
        /// Alignment simulates a natural flocking behaviour also known as "copy". Every entity of the swarm steers in the same direction as their surrounding nearby entities.  
        /// </summary>
        /// <param name="entities">The entities of a swarm to which the behaviour should be applied.</param>
        /// <param name="radius">The radius of the circular area around each single entity in which the behaviour is effectiv</param>
        /// <param name="weight">Defines how "important" this behaviour is compared to the other behaviours. Affects (scales) the magnitude of the steering vector as a multiplier. 0 means the behaviour is not applied.</param>
        public void Align(List<SwarmEntity> entities, float radius, float weight = 1.0f)
        {
            if (entities == null)
                return;

            Vector2 sum = new Vector2();
            int count = 0;

            // the entities desired velocity is the average velocity of its neighbors
            // so we add up all the velocities and devide them by the total to calculate the average velocity
            foreach (SwarmEntity entity in entities)
            {
                // distance between this entity and the currently processed entity in the list
                float distance = (entity.transform.position - transform.position).magnitude;

                if ((distance > 0) && (distance < radius))
                {
                    sum += entity.velocity;
                    count++;
                }
            }

            if (count > 0)
            {
                sum /= count;

                // we desire to go in that direction at maximum speed
                sum = sum.normalized * maxSpeed;

                // Reynold's steering formular
                Vector2 steer = sum - velocity;

                // clamp the steering to a max force 
                steer = Vector2.ClampMagnitude(steer, maxForce);

                // weight the behavior (how important is it compared to the other behaviors applied to the entity)
                steer *= weight;

                ApplyForce(steer);
            }
        }

        public void Cohesion()
        {
            Cohesion(_swarm.entities);
        }

        public void Cohesion(List<SwarmEntity> entities)
        {
            Cohesion(entities, _swarm.cohesion.radius, _swarm.cohesion.weight);
        }

        /// <summary>
        /// Cohesion simulates a natural flocking behaviour also known as "center". Every entity of the swarm steers towards the center of their surrounding nearby entities.  
        /// </summary>
        /// <param name="entities">The entities of a swarm to which the behaviour should be applied.</param>
        /// <param name="radius">The radius of the circular area around each single entity in which the behaviour is effectiv</param>
        /// <param name="weight">Defines how "important" this behaviour is compared to the other behaviours. Affects (scales) the magnitude of the steering vector as a multiplier. 0 means the behaviour is not applied.</param>
        public void Cohesion(List<SwarmEntity> entities, float radius, float weight = 1.0f)
        {
            if (entities == null)
                return;

            Vector2 sum = new Vector2();
            int count = 0;

            foreach (SwarmEntity entity in entities)
            {
                float distance = (entity.transform.position - transform.position).magnitude;

                if ((distance > 0) && (distance < radius))
                {
                    // the cohesion function is almost identical to the alignment function
                    // but instead of calculating the average velocity we are calculating the average position of our neightbors and steer towards it
                    sum += (Vector2)entity.transform.position;
                    count++;
                }
            }

            if (count > 0)
            {
                sum /= count;
                // the average location of the neighbors becomes the desired seek target of our entity 
                Seek(sum, weight);
            }
        }

        public void Hunt()
        {
            Hunt(_swarm.preyType, _swarm.hunt.radius, _swarm.hunt.weight);
        }

        /// <summary>
        /// Hunt simulates a natural pursuit behaviour. The predator swarm performes alignment and cohesion with the entities of the prey swarm to mirror their collective steering direction and to target their swarm center.
        /// </summary>
        /// <param name="preyType">Defines which type of entities should be hunted by the predator swarm</param>
        /// <param name="radius">The radius of the circular area around each single entity in which the behaviour is effectiv</param>
        /// <param name="weight">Defines how "important" this behaviour is compared to the other behaviours. Affects (scales) the magnitude of the steering vector as a multiplier. 0 means the behaviour is not applied.</param>
        public void Hunt(string preyType, float radius, float weight = 1.0f)
        {
            if (Swarm.entitiesByType.ContainsKey(preyType))
            {
                Align(Swarm.entitiesByType[preyType], radius, weight);
                Cohesion(Swarm.entitiesByType[preyType], radius, weight);
            }
        }

        public void Flee(string fleeType)
        {
            Flee(fleeType, _swarm.flee.radius, _swarm.flee.weight);
        }

        /// <summary>
        /// Flee simulates a natural flight behaviour. It uses the Seperation behaviour to avoid colliding with swarm entities that are not part of the own swarm.
        /// </summary>
        /// <param name="fleeType">Defines the type of entities the swarm should avoid</param>
        /// <param name="radius">The radius of the circular area around each single entity in which the behaviour is effectiv</param>
        /// <param name="weight">Defines how "important" this behaviour is compared to the other behaviours. Affects (scales) the magnitude of the steering vector as a multiplier. 0 means the behaviour is not applied.</param>
        public void Flee(string fleeType, float radius, float weight = 1.0f)
        {
            if (Swarm.entitiesByType.ContainsKey(fleeType))
            {
                Seperate(Swarm.entitiesByType[fleeType], radius, weight);
            }
        }
    }
}