# Hyvemind

## Overview
Hyvemind is a small and free library to simulate **swarm behaviour** of living creatures (e.g. fish swarms or bird flocks) for **2D applications** created with Unity3D. It was created by applying basic [concepts for autonomous agents](https://mitpress.mit.edu/books/vehicles) by Valentino Braitenberg and by using the [boids steering formular](http://www.red3d.com/cwr/steer/gdc99/) by Craig Reynolds.

## Download

You can download the UnityPackage [here](https://bitbucket.org/cybersquid/hyvemind/downloads/hyvemind.unitypackage).

## Usage
Setting up Hyvemind in your own project is fairly easy. 

#### 1. Setup the Swarm
First you need to add the **Swarm** script to a GameObject in you scene (this might as well be an empty GameObject). 
Here you can set the SwarmEntity prefab, the number of SwarmEntities and their spawn area. The swarm behaviours, which will be explained in greater detail further below, are also set in this object.

![setup swarm][setup-swarm]

#### 2. Add a SwarmEntity
Now you need to define of what typ of "creatures" your swarm should consist. Pretty much any GameObject can be used for this purpose as long as it has the **SwarmEntity** script attached to it. Create a prefab of this SwarmEntity object and pull it to the "Entity Prefab" field in the inspector of the swarm object. If you run the application the swarm will use this prefab to instantiate the predefined number of swarm entities in the spawn area. All spawned SwarmEntities will apply the bahaviours that have been set up in the swarm and act like a flock of living objects making movement decisions depending on the position and behaviour of their neighbour SwarmEntities. 

It is also possible to lable SwarmEntities with a custom type, like _"prey"_ or _"predator"_ to set up more complex hunt or flee behaviours for a swarm.

## Features

### Swarm Behaviour
A swarm describes an emergent phenomenon in which a group of individual entities appears to act as one greater organism (also called super-organism). This happens because every entity constantly deceides its next action on simple behaviour rules that it uses to calculate a movement steer depending on the current actions and movements of its neighbour entities in the swarm. 

Hyvemind lets you adjust all its swarm behaviours during runtime. You can play around with them and watch your creatures vividly interact with eachother.

### Movement by Steering
Each update cycle the behaviour rules are used to calculate a vector towards a desired target for every single entity. This describes the optimal direction, that should be followed by the entity to perfectly flock with the swarm. But natural behaviour is not perfect and every simulation that would just use the desired vector as a steer would look "robotic" and lifeless. So in order to make the movement of the SwarmEntities appear more "lifelike" and natural Hyvemind uses the  [boids steering formular](http://www.red3d.com/cwr/boids/) by Craig Reynolds. It calculates the final steer of an entity from its current velocity and its desired vector.

You can define the steering ability of your SwarmEntities by setting **Max Entity Speed** (max length of the desired vector) and **Max Entity Steering Force** (max length of the final steering vector) in the Movement section of the inspector of your Swarm object.

### Weighted Behaviours
Most of the Behaviours in Hyvemind are using a weight and a radius to define their influence on a SwarmEntity.

- **weight**: Describes how "important" this behaviour is compared to others. It directly affects (scales) the magnitude of the steering vector as a multiplier. Setting it to zero (0) means the behaviour will not be applied.

- **radius**: The radius of the circular area around each single entity in which the behaviour is effectiv. Only entities inside this area are considered as neighbours that affect the steering calculation.

### Flocking

![simple flocking][flocking]

The most basic steering behaviours are seperation, alignment and cohesion. They can be set in the inspector of the Swarm object to create a simple flock of moving entities.

- **Seperation** - steer to avoid a colliding with other nearby entities
- **Alignment** - steer to move in the same direction as nearby entities
- **Cohesion** - steer to move towards the center of nearby entities. 

### Seeking

![chasing target][seeking]

Any GameObject ca be set as a seeking target for the swarm. Just drag it in the Target field in the Seeking Behavior section in the inspector of the swarm script. All SwarmEntities will chase this target no matter where it is located in the scene. The seeking behaviour can be weighted and it is also possible to set an arival radius that defines a circular area around the target in which a SwarmEntity is slowing down its movement until it finally reaches the position of the target.

![inspector seeking][inspector-seeking]

### Predator-Prey-Relationship

![predator prey][predator-prey-relationship]

Hyvemind can be used to simulate models of simple predator-prey-relationships. Swarms can hunt other swarms and SwarmEntities are able to flee from predefined obstacles or predators.

#### Hunt
In contrast to the seeking behaviour, which enables the swarm to chase a single target, the hunting behaviour can be used to pursuit a whole swarm. Internally it is build uppon the Alignment and Cohesion behaviours to make the entities of the predator swarm flock with the entities of the prey swarm. If and how the prey reacts to this persecution is up to you. Hunting is a weighted behaviour and the prey can be defined by entering the custom entity type of the entities of the prey swarm in the field **Prey Type**.

![inspector predator][inspector-predator]

#### Flee
The flee behaviour uses the seperation ability of the SwarmEntities to avoid entities of other swarms (steer away from them). It can have multiple Flee Types (entity types of the entities of the predator swarms) to escape from multiple types of predators.

![inspector flee][inspector-prey]

### Particle Swarm (experimental)

![particle swarm][particle-swarm]

Since the basic swarm behaviours only affect the movement of the SwarmEntities it is also possible to inject their resulting steers into the velocity of particles in [Unitys Shuriken particle system](https://docs.unity3d.com/ScriptReference/ParticleSystem.html). To do so you can use Hyveminds ParticleSwarm script and add it to any GameObject that has a particle system attached to it. Now you can influence the particle with different swarm behaviours like **flocking** (seperation, alignment, cohesion), **seeking** and **avoidance** (flee).

##### important!
This feature is currently in an experimental state as it still suffers from some performance issues. So be warned and use at own risk (aside from that it's pretty fun to play with, so you might as well just check it out and see for yourself).

## Examples
The unity package of Hyvemind provides example scenes for every swarm behaviour (including the particle swarm) in the folder hyvemind/examples.

## Games
Hyvemind will be used in the upcoming Cybersquid game [Pacific Standard Slime](http://www.cybersquid.net/2017/01/update-01-introducing/)!

## License
Hyvemind is released under the [MIT license](https://bitbucket.org/cybersquid/hyvemind/raw/83cc8bc6138e0765116b6efecf983d6ee276b4ab/LICENSE).


[//]: # (Images)
[setup-swarm]: https://bytebucket.org/cybersquid/hyvemind/raw/866862615921db31a6e124eb796a3252ae16d7c0/common/img/setup-swarm.gif "Setup Swarm"

[flocking]: https://bytebucket.org/cybersquid/hyvemind/raw/bcb9d4bb2dee4ed81b907e7026db562ac1d55dcf/common/img/simple-flocking.gif "Simple Flocking"

[seeking]: https://bytebucket.org/cybersquid/hyvemind/raw/866862615921db31a6e124eb796a3252ae16d7c0/common/img/swarm-chasing-target.gif "Swarm Chasing Target"
[inspector-seeking]: https://bytebucket.org/cybersquid/hyvemind/raw/866862615921db31a6e124eb796a3252ae16d7c0/common/img/inspector_seeking.png "Seeking Behaviour Inspector"

[predator-prey-relationship]: https://bytebucket.org/cybersquid/hyvemind/raw/866862615921db31a6e124eb796a3252ae16d7c0/common/img/predator-prey.gif "Predator-Prey-Relationship"
[inspector-predator]: https://bytebucket.org/cybersquid/hyvemind/raw/866862615921db31a6e124eb796a3252ae16d7c0/common/img/inspector_predator.png "Hunting Behaviour Inspector"
[inspector-prey]: https://bytebucket.org/cybersquid/hyvemind/raw/866862615921db31a6e124eb796a3252ae16d7c0/common/img/inspector_prey.png "Flee Behaviour Inspector"

[particle-swarm]: https://bytebucket.org/cybersquid/hyvemind/raw/866862615921db31a6e124eb796a3252ae16d7c0/common/img/particle-swarm.gif "Particle Swarm"